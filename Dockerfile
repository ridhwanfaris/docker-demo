FROM golang:1.16-alpine as build-env

WORKDIR /go/src/demo

COPY go.mod /go/src/demo/
COPY go.sum /go/src/demo/

RUN go mod download

COPY *.go /go/src/demo/

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/demo

EXPOSE 8282

CMD [ "/go/bin/demo" ]

FROM scratch
COPY --from=build-env /go/bin/demo /go/bin/demo
ENTRYPOINT [ "/go/bin/demo" ]