module docker-sample

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.4
	github.com/go-chi/cors v1.2.0
)
