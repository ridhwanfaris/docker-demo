package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"
)

type SampleResponse struct {
	Message string `json:"message"`
}

type LoginResponse struct {
	Username     string `json:"username"`
	Role         string `json:"role"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type BaseResponse struct {
	Code    int         `json:"code"`
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func main() {
	r := RegisterRoute()
	fmt.Println("Start server...")
	http.ListenAndServe(":8282", r)
}

func RegisterRoute() *chi.Mux {
	r := chi.NewRouter()
	// Basic CORS
	// for more ideas, see: https://developer.github.com/v3/#cross-origin-resource-sharing
	r.Use(cors.Handler(cors.Options{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"https://*", "http://*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	r.Get("/", HomeHandler)
	return r
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)

	loginRes := LoginResponse{
		Username:     "ridhwan.faris11@gmail.com",
		Role:         "OFFICER",
		AccessToken:  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NfdXVpZCI6IjYwNGViMzk0LWNmN2MtNGY1MS1iNTc2LThiY2ZmYWYxNGRlNiIsImNoYW5uZWwiOiJpbml2ZW5fYmFja19vZmZpY2UiLCJlbWFpbCI6InJpZGh3YW4uZmFyaXMxMUBnbWFpbC5jb20iLCJleHAiOjE2MzE3ODQ3MjUsImlkIjoiMSIsInJvbGUiOiJPRkZJQ0VSIn0.yQjFfAfFy6T4Tuo6JJGbnXWbaXKXdtjkAKH5qOCe9hY",
		RefreshToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NfdXVpZCI6IjYwNGViMzk0LWNmN2MtNGY1MS1iNTc2LThiY2ZmYWYxNGRlNiIsImNoYW5uZWwiOiJpbml2ZW5fYmFja19vZmZpY2UiLCJlbWFpbCI6InJpZGh3YW4uZmFyaXMxMUBnbWFpbC5jb20iLCJleHAiOjE2MzE3ODQ3MjUsImlkIjoiMSIsInJvbGUiOiJPRkZJQ0VSIn0.yQjFfAfFy6T4Tuo6JJGbnXWbaXKXdtjkAKH5qOCe9hY",
	}

	res := BaseResponse{
		Code:    200,
		Status:  "success",
		Message: "login success",
		Data:    loginRes,
	}

	json.NewEncoder(w).Encode(res)
}

func ResponseWithJson(w http.ResponseWriter, httpCode int, status string, message string, data interface{}) {
	res := BaseResponse{
		Code:    httpCode,
		Status:  status,
		Message: message,
		Data:    data,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpCode)
	json.NewEncoder(w).Encode(res)
}
